# Measurements

First I measure the power supply as it is, without making changes.

## Output voltage

0 - 15 V range

Increasing from 0 to 15.3 V - the output voltage is stable, and increasing with changes in R27.

Above 15.3 V (approximately): the voltage oscillates, then slowly (very slowly) settles down. Peaks at about 30 V.


0 - 30 V range

Increasing from 0 to 15 V (approximately): the outpu voltage is stable, increases with changes in R27.

Above 15 V (approximately): the output voltage oscillates, then settles down.

## Vref voltage

Measured at 5.2 V output: 7.966 V, at 10 V output: 7.967 V, at 14.88 V output: 7.967 V

## 15 V over D5 (zener)

Measures 15.2 V

# Changes

R4: 1k -> 10k. The measurements are about the same, the instability / oscillation above 15 V output too.

Ajusting the reference voltage down to 6.1 V didn't show an improvement either - the output voltage is lower,
and the instability occurs at a lower output voltage.