# Mascot 719 lab power supply

This is just a re-creation of the schematics for the Mascot 719 in KiCAD, based on the schematics in 
[Mascot 719 Schematics, Reverse Engineering and Repair](https://axotron.se/blog/mascot-719-schematics-reverse-engineering-and-repair/). 
We have a (slightly modified) Mascot 719 at our makerspace.

Our 719 has some components that have different values from the schematic.

Differences from the schematic:

R4 = 1k (10k in schematic)

R11 = 12k (10k in schematic)

R31 = 4k7 (2k2 in schematic)

Other relevant links:

https://projects.dahlhjarup.dk/2018/08/11/repair-mascot-719-power-supply-0/

http://operationalsmoke.blogspot.com/2013/09/mascot-719.html

https://www.mascot.no/products?catalog=24&category=1325&product=235
