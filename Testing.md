# Testing

R27 - voltage adjustment potentiometer - appears to be in working order

Rextra1 - extra potentiometer - 10k multiturn - appears to be in working order. Connected between the cw (top) end of R27 and R28.

T1 - 2N3055 - NPN, measured with my cheap component tester, checks out hFE = 80

T2 - BD136 - PNP, unsoldered and measured with my cheap component tester, checks out, hFE = 153. Soldered it back in.

T3 - BC547B - NPN, unsoldered and measured with my cheap component tester, checks out, hFE = 428. Soldered back in.

D1 - D4 - 1N5406 - diodes - tested in circuit with my cheap component tester, all reports "2 diodes" so are probably ok.
